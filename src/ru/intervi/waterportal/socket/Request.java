package ru.intervi.waterportal.socket;

import java.util.AbstractMap;
import java.util.Map;
import java.util.UUID;

public class Request {
    public static String requestMove(UUID uuid, String server) {
        return uuid.toString() + "=" + server;
    }

    public static Map.Entry<UUID, String> parseMoveRequest(String request) {
        String[] result = request.split("=");
        return new AbstractMap.SimpleEntry<>(UUID.fromString(result[0].trim()), result[1].trim());
    }
}
