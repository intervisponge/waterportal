package ru.intervi.waterportal.socket;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;

public class SocketClient {
    private final String HOST;
    private final int PORT, TIMEOUT;

    public SocketClient(String host, int port, int timeout) {
        HOST = host;
        PORT = port;
        TIMEOUT = timeout;
    }

    public void request(String data) throws IOException {
        Socket socket = null;
        try {
            socket = new Socket(HOST, PORT);
            socket.setSoTimeout(TIMEOUT);
            socket.getOutputStream().write(data.getBytes(Charset.forName("UTF-8")));
            socket.getOutputStream().flush();
            socket.shutdownOutput();
        } finally {
            if (socket != null) socket.close();
        }
    }
}
