package ru.intervi.waterportal.cuboid;

public class Point {
    public final int X, Y, Z;

    public Point(int x, int y, int z) {
        X = x;
        Y = y;
        Z = z;
    }

    @Override
    public String toString() {
        return "(" + X + ", " + Y + ", " + Z + ")";
    }
}
