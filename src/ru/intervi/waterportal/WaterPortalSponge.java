package ru.intervi.waterportal;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;
import ru.intervi.waterportal.cuboid.Cuboid;
import ru.intervi.waterportal.cuboid.Point;
import ru.intervi.waterportal.socket.Request;
import ru.intervi.waterportal.socket.SocketClient;

import javax.annotation.Nonnull;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Plugin(
        id = "waterportal", name = "WaterPortal", version = "1.0",
        authors = {"InterVi"},
        description = "Teleport to other servers with bungee."
)
public class WaterPortalSponge {
    @Inject
    @DefaultConfig(sharedRoot = true)
    private Path defaultConfig;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    private CommentedConfigurationNode config;
    private final Map<String, String> MAP = new HashMap<>();
    private SocketClient socketClient;

    private void setupConfig(){
        try {
            if (!Files.exists(defaultConfig)) {
                Files.createFile(defaultConfig);
                config = configManager.createEmptyNode();
                config.getNode("socket", "enabled").setValue(false);
                config.getNode("socket", "host").setValue("127.0.0.1");
                config.getNode("socket", "port").setValue(25520);
                config.getNode("socket", "timeout").setValue(2000);
                config.getNode("commands", "test").setValue("help");
                config.getNode("translate", "tp").setValue("teleport to %s...");
                config.getNode("translate", "error").setValue("Plugin error. Please report admin.");
                configManager.save(config);
            } else {
                config = configManager.load();
                List<String> list = config.getNode("points").getList(TypeToken.of(String.class));
                if (list.isEmpty()) return;
                for (String line : list) {
                    String[] data = line.split("=");
                    MAP.put(data[0], data[1]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createSocket() {
        if (config.getNode("socket", "enabled").getBoolean()) {
            socketClient = new SocketClient(
                    config.getNode("socket", "host").getString(),
                    config.getNode("socket", "port").getInt(),
                    config.getNode("socket", "timeout").getInt()
            );
        } else {
            socketClient = null;
        }
    }

    private void savePoints() {
        try {
            List<String> list = new ArrayList<>();
            for (Map.Entry<String, String> entry : MAP.entrySet()) {
                list.add(entry.getKey() + '=' + entry.getValue());
            }
            config.getNode("points").setValue(list);
            configManager.save(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        setupConfig();
        createSocket();
        CommandSpec reload = CommandSpec.builder()
                .description(Text.of("Config reload command."))
                .permission("waterportal.cmd.admin.reload")
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    MAP.clear();
                    setupConfig();
                    src.sendMessage(Text.of("config reloaded"));
                    return CommandResult.success();
                })
                .build();
        CommandSpec add = CommandSpec.builder()
                .description(Text.of("Add portal."))
                .permission("waterportal.cmd.admin.add")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.integer(Text.of("radius"))
                )
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    String name = args.<String>getOne("name").get();
                    int radius = args.<Integer>getOne("radius").get();
                    workRadius(player.getWorld(), player.getLocation().getBlockPosition(), radius, name, false);
                    savePoints();
                    src.sendMessage(Text.of(name + " portal added"));
                    return CommandResult.success();
                })
                .build();
        CommandSpec remove = CommandSpec.builder()
                .description(Text.of("Remove portal."))
                .permission("waterportal.cmd.admin.remove")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.integer(Text.of("radius"))
                )
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    String name = args.<String>getOne("name").get();
                    int radius = args.<Integer>getOne("radius").get();
                    workRadius(player.getWorld(), player.getLocation().getBlockPosition(), radius, name, true);
                    savePoints();
                    src.sendMessage(Text.of(name + " portal removed"));
                    return CommandResult.success();
                })
                .build();
        Sponge.getCommandManager().register(this, reload, "wp-reload", "waterportal-reload");
        Sponge.getCommandManager().register(this, add, "wp-add", "waterportal-add");
        Sponge.getCommandManager().register(this, remove, "wp-remove", "waterportal-remove");
    }

    private void workRadius(World world, Vector3i vec, int radius, String server, boolean remove) {
        Vector3i pos1 = vec.add(radius, radius, radius);
        Vector3i pos2 = vec.add(-radius, -radius, -radius);
        for (Point point : new Cuboid(pos1.getX(), pos1.getY(), pos1.getZ(), pos2.getX(), pos2.getY(), pos2.getZ())) {
            String typeName = world.getBlock(point.X, point.Y, point.Z).getType().getName();
            if (
                    !typeName.equals(BlockTypes.WATER.getName()) &&
                            !typeName.equals(BlockTypes.FLOWING_WATER.getName())
            ) {
                continue;
            }
            if (remove) MAP.remove(point.toString());
            else MAP.put(point.toString(), server);
        }
    }

    @Listener
    public void onMove(MoveEntityEvent event) {
        if (!event.getCause().first(Player.class).isPresent()) return;
        if (
                event.getFromTransform().getLocation().getBlockPosition()
                        .distanceSquared(event.getToTransform().getLocation().getBlockPosition())
                        < 1
        ) {
            return;
        }
        String typeName = event.getToTransform().getLocation().getBlock().getType().getName();
        if (
                !typeName.equals(BlockTypes.WATER.getName()) &&
                        !typeName.equals(BlockTypes.FLOWING_WATER.getName())
        ) {
            return;
        }
        Player player = event.getCause().first(Player.class).get();
        String cords = event.getToTransform().getLocation().getBlockPosition().toString();
        if (!MAP.containsKey(cords)) return;
        String server = MAP.get(cords);
        String cmd = config.getNode("commands", server).getString();
        if (cmd != null) {
            Sponge.getCommandManager().process(player, cmd);
            return;
        }
        player.sendMessage(Text.of(String.format(config.getNode("translate", "tp").getString(), server)));
        if (config.getNode("socket", "enabled").getBoolean()) {
            if (socketClient == null) return;
            try {
                socketClient.request(Request.requestMove(player.getUniqueId(), server));
            } catch (Exception e) {
                e.printStackTrace();
                player.sendMessage(Text.of(config.getNode("translate", "error").getString()));
            }
        }
    }
}
