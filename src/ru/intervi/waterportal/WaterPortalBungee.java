package ru.intervi.waterportal;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.command.ConsoleCommandSender;
import ru.intervi.waterportal.socket.Request;
import ru.intervi.waterportal.socket.SocketServer;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

public class WaterPortalBungee extends Plugin implements Listener, SocketServer.SoListener {
    private final File PROP_FILE = new File(new File(
            this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()
    ).getAbsoluteFile().getParentFile(), "WaterPortals.properties");
    private SocketServer server;
    private Thread thread;

    public WaterPortalBungee() {
        if (!PROP_FILE.isFile()) {
            saveDefault();
            System.out.println("Create properties file. Edit and reload.");
        }
    }

    @Override
    public void onEnable() {
        createServer();
        getProxy().getPluginManager().registerCommand(this, new ReloadCmd("hba-reload"));
    }

    @Override
    public void onDisable() {
        stopServer();
    }

    private void stopServer() {
        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (thread != null) {
            try {
                thread.interrupt();
                thread = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createServer() {
        try {
            Map<String, String> map = load();
            server = new SocketServer(
                    map.get("host"),
                    Integer.parseInt(map.get("port")),
                    Integer.parseInt(map.get("maxconn")),
                    Integer.parseInt(map.get("timeout")),
                    this
            );
            server.start();
            thread = new Thread(() -> server.work());
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Map<String, String> load() {
        HashMap<String, String> result = new HashMap<>();
        try {
            Properties prop = new Properties();
            prop.load(new FileReader(PROP_FILE));
            for (String key : prop.stringPropertyNames()) {
                result.put(key, prop.getProperty(key));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void save(Map<String, String> map) {
        try {
            Properties prop = new Properties();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                prop.setProperty(entry.getKey(), entry.getValue());
            }
            prop.store(new FileWriter(PROP_FILE), "socket server settings");
        }
        catch(Exception e) {e.printStackTrace();}
    }

    private void saveDefault() {
        HashMap<String, String> map = new HashMap<>();
        map.put("host", "127.0.0.1");
        map.put("port", "25520");
        map.put("maxconn", "100");
        map.put("timeout", "2000");
        save(map);
    }

    @Override
    public String request(String data) {
        try {
            Map.Entry<UUID, String> entry = Request.parseMoveRequest(data);
            getProxy().getPlayer(entry.getKey()).connect(getProxy().getServers().get(entry.getValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class ReloadCmd extends Command {
        ReloadCmd(String name) {
            super(name);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void execute(CommandSender commandSender, String[] strings) {
            if (!(commandSender instanceof ConsoleCommandSender)) {
                commandSender.sendMessage("only console");
                return;
            }
            stopServer();
            createServer();
            commandSender.sendMessage("prop reloaded and socket server recreated");
        }
    }
}
